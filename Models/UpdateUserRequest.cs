using System.ComponentModel.DataAnnotations;

namespace AuthMicroservice.Models
{
    public class UpdateUserRequest
    {
        [Required]
        public string? Username { get; set; }

        [Required]
        [EmailAddress]
        public string? Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string? Password { get; set; }
    }
}