using Microsoft.AspNetCore.Identity;

namespace AuthMicroservice.Models
{
    public class User : IdentityUser
    {
        // Add additional properties as needed
        public string Token { get; set; }
    }
}