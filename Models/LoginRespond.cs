namespace AuthMicroservice.Models
{
    public class LoginResponse
    {
        public string Token { get; set; }
        // Add any additional properties you want to include in the login response
    }
}