using AuthMicroservice.Models;

namespace AuthMicroservice.Repositories.Interfaces;
public interface IAuthRepository
{
    Task<User> FindByEmailAsync(string email);
    Task<bool> CheckPasswordAsync(User user, string password);
    Task<bool> SignInAsync(User user, string password, bool rememberMe);
}