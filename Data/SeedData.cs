using AuthMicroservice.Models;
using Microsoft.AspNetCore.Identity;

namespace AuthMicroservice.Data
{
    public static class SeedData
    {
        public static void Initialize(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            SeedRoles(roleManager);
            SeedUsers(userManager);
        }

        private static void SeedRoles(RoleManager<IdentityRole> roleManager)
        {
            // Check if the "Admin" role exists, if not create it
            if (!roleManager.RoleExistsAsync("Admin").Result)
            {
                IdentityRole role = new IdentityRole
                {
                    Name = "Admin"
                };

                roleManager.CreateAsync(role).Wait();
            }

            // Add more roles as needed
        }

        private static void SeedUsers(UserManager<User> userManager)
        {
            // Check if the admin user exists, if not create it
            if (userManager.FindByNameAsync("admin").Result == null)
            {
                User user = new User
                {
                    UserName = "admin"
                };

                IdentityResult result = userManager.CreateAsync(user, "password").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Admin").Wait();
                }
            }

            // Add more users as needed
        }
    }
}
