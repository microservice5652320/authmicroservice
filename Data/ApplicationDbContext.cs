using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using AuthMicroservice.Models;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace AuthMicroservice.Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Add your entity configurations and other modelBuilder configurations

            SeedData(modelBuilder);
        }

        private void SeedData(ModelBuilder modelBuilder)
        {
            // Seed roles
            modelBuilder.Entity<IdentityRole>().HasData(
                new IdentityRole { Id = "1", Name = "SuperAdmin", NormalizedName = "SUPERADMIN" },
                new IdentityRole { Id = "2", Name = "Admin", NormalizedName = "Admin" },
                new IdentityRole { Id = "3", Name = "User", NormalizedName = "USER" }
            );

            // Seed users
            var hasher = new PasswordHasher<User>();
            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Id = "1",
                    UserName = "admin",
                    NormalizedUserName = "ADMIN@EXAMPLE.COM",
                    Email = "admin@example.com",
                    NormalizedEmail = "ADMIN@EXAMPLE.COM",
                    EmailConfirmed = true,
                    PasswordHash = hasher.HashPassword(new User(), "Admin123$"), // Replace with a secure password
                    SecurityStamp = string.Empty,
                    Token = ""
                },
                new User
                {
                    Id = "2",
                    UserName = "zhihow",
                    NormalizedUserName = "ZHIHOW",
                    Email = "zhihow2002@yahoo.com.tw",
                    NormalizedEmail = "ZHIHOW2002@YAHOO.COM.TW",
                    EmailConfirmed = true,
                    PasswordHash = hasher.HashPassword(new User(), "zhihow123"), // Replace with a secure password
                    SecurityStamp = string.Empty,
                    Token = ""
                },
                new User
                {
                    Id = "3",
                    UserName = "user",
                    NormalizedUserName = "USER",
                    Email = "user@example.com",
                    NormalizedEmail = "USER@EXAMPLE.COM",
                    EmailConfirmed = true,
                    PasswordHash = hasher.HashPassword(new User(), "User123$"), // Replace with a secure password
                    SecurityStamp = string.Empty,
                    Token = ""
                }
            );

            // Assign roles to users
            modelBuilder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string> { UserId = "1", RoleId = "2" }, // Admin user assigned to Admin role
                new IdentityUserRole<string> { UserId = "2", RoleId = "1" },  // Regular user assigned to Super Admin role
                new IdentityUserRole<string> { UserId = "3", RoleId = "3" }  // Regular user assigned to User role
            );

            // Seed claims
            modelBuilder.Entity<IdentityUserClaim<string>>().HasData(
                new IdentityUserClaim<string> { Id = 1, UserId = "1", ClaimType = ClaimTypes.Country, ClaimValue = "USA" },
                new IdentityUserClaim<string> { Id = 2, UserId = "1", ClaimType = ClaimTypes.Role, ClaimValue = "Admin" },
                new IdentityUserClaim<string> { Id = 3, UserId = "1", ClaimType = "aud", ClaimValue = "https://yourdomain.com" },
                // Add more claims as needed
                new IdentityUserClaim<string> { Id = 4, UserId = "2", ClaimType = ClaimTypes.Country, ClaimValue = "MALAYSIA" },
                new IdentityUserClaim<string> { Id = 5, UserId = "2", ClaimType = ClaimTypes.Role, ClaimValue = "SuperAdmin" },
                new IdentityUserClaim<string> { Id = 6, UserId = "2", ClaimType = "aud", ClaimValue = "https://yourdomain.com" },

                new IdentityUserClaim<string> { Id = 7, UserId = "3", ClaimType = ClaimTypes.Country, ClaimValue = "USA" },
                new IdentityUserClaim<string> { Id = 8, UserId = "3", ClaimType = ClaimTypes.Role, ClaimValue = "User" },
                new IdentityUserClaim<string> { Id = 9, UserId = "3", ClaimType = "aud", ClaimValue = "https://yourdomain.com" }
            );

            // Seed role claims
            modelBuilder.Entity<IdentityRoleClaim<string>>().HasData(
                new IdentityRoleClaim<string> { Id = 1, RoleId = "1", ClaimType = ClaimTypes.Role, ClaimValue = "AdminClaim1" },
                new IdentityRoleClaim<string> { Id = 2, RoleId = "1", ClaimType = ClaimTypes.Role, ClaimValue = "AdminClaim2" },
                new IdentityRoleClaim<string> { Id = 3, RoleId = "2", ClaimType = ClaimTypes.Role, ClaimValue = "SuperAdminClaim1" },
                new IdentityRoleClaim<string> { Id = 4, RoleId = "2", ClaimType = ClaimTypes.Role, ClaimValue = "SuperAdminClaim2" },
                new IdentityRoleClaim<string> { Id = 5, RoleId = "3", ClaimType = ClaimTypes.Role, ClaimValue = "UserClaim1" },
                new IdentityRoleClaim<string> { Id = 6, RoleId = "3", ClaimType = ClaimTypes.Role, ClaimValue = "UserClaim2" }


            // Add more role claims as needed
            );

        }

    }
}