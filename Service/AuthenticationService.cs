using AuthMicroservice.Models;
using Microsoft.AspNetCore.Identity;

namespace AuthMicroservice.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthenticationService : IAuthenticationService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly ITokenProvider _tokenProvider;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="signInManager"></param>
        /// <param name="tokenProvider"></param>
        public AuthenticationService(UserManager<User> userManager, SignInManager<User> signInManager, ITokenProvider tokenProvider)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _tokenProvider = tokenProvider;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="audience"></param>
        /// <param name="issuer"></param>
        /// <param name="tokenExpirationMinutes"></param>
        /// <returns></returns>

        public async Task<AuthenticationResult> AuthenticateAsync(string email, string password, string audience, string issuer, int tokenExpirationMinutes)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return AuthenticationResult.Failure("Invalid email or password.");
            }

            var result = await _signInManager.PasswordSignInAsync(user, password, false, lockoutOnFailure: false);
            if (!result.Succeeded)
            {
                return AuthenticationResult.Failure("Invalid email or password.");
            }

            var token = _tokenProvider.GenerateToken(user.Id, user.Email, audience, issuer, tokenExpirationMinutes);

            return AuthenticationResult.Success(token);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IAuthenticationService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="audience"></param>
        /// <param name="issuer"></param>
        /// <param name="tokenExpirationMinutes"></param>
        /// <returns></returns>
        Task<AuthenticationResult> AuthenticateAsync(string email, string password, string audience, string issuer, int tokenExpirationMinutes);
    }
    /// <summary>
    /// 
    /// </summary>

    public class AuthenticationResult
    {
        /// <summary>
        /// Succeeded
        /// </summary>
        /// <value>true</value>
        public bool Succeeded { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        public string? Token { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        public string? ErrorMessage { get; private set; }

        private AuthenticationResult(bool succeeded, string? token, string? errorMessage)
        {
            Succeeded = succeeded;
            Token = token;
            ErrorMessage = errorMessage;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>

        public static AuthenticationResult Success(string token)
        {
            return new AuthenticationResult(true, token, null);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>

        public static AuthenticationResult Failure(string errorMessage)
        {
            return new AuthenticationResult(false, null, errorMessage);
        }
    }
}
