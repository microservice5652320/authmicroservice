

using AuthMicroservice.Models;

namespace AuthMicroservice.Services
{
    public interface IAuthService
    {
        Task<string> GenerateJwtToken(string email, string password);
        Task<User> FindByEmailAsync(string email);
    }
}
