using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using AuthMicroservice.Models;
using AuthMicroservice.Service;

namespace AuthMicroservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly UserService _userService;
        /// <summary>
        /// 
        /// 
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="userService"></param>
        public UserController(UserManager<User> userManager, UserService userService)
        {
            _userManager = userManager;
            _userService = userService;
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([FromBody] CreateUserRequest model)
        {
            if (model is null)
            {
                throw new ArgumentNullException(nameof(model));
            }

            if (string.IsNullOrEmpty(model.Password))
            {
                return BadRequest("Password is required.");
            }

            var user = new User { UserName = model.Username, Email = model.Email };

            var result = await _userService.CreateUser(user, model.Password);

            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return BadRequest(result.Errors);
            }
        }


        [HttpGet("getuserdatabyid")]
        [Authorize(Roles = "Admin,SuperAdmin")]
        public async Task<IActionResult> GetUserData(string id)
        {
            var authorizationHeader = HttpContext.Request.Headers["Authorization"];
            var user = await _userService.GetUserById(id);

            if (user != null)
            {
                return Ok(user);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Update(string id, [FromBody] UpdateUserRequest model)
        {
            var user = await _userService.GetUserById(id);

            if (user != null)
            {
                user.UserName = model.Username;
                user.Email = model.Email;

                // Update password if provided
                if (!string.IsNullOrEmpty(model.Password))
                {
                    var passwordValidator = new PasswordValidator<User>();
                    var passwordValidationResult = await passwordValidator.ValidateAsync(_userManager, user, model.Password);

                    if (passwordValidationResult.Succeeded)
                    {
                        user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, model.Password);
                    }
                    else
                    {
                        return BadRequest(passwordValidationResult.Errors);
                    }
                }

                var result = await _userService.UpdateUser(user);

                if (result.Succeeded)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest(result.Errors);
                }
            }
            else
            {
                return NotFound();
            }
        }


        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(string id)
        {
            var user = await _userService.GetUserById(id);

            if (user != null)
            {
                var result = await _userService.DeleteUser(user);

                if (result.Succeeded)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest(result.Errors);
                }
            }
            else
            {
                return NotFound();
            }
        }
    }
}
