using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using AuthMicroservice.Models;

namespace AuthMicroservice.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        protected readonly UserManager<User> _userManager;
        protected readonly SignInManager<User> _signInManager;
        private readonly ILogger<AuthController> _logger;

        public AuthController(IConfiguration configuration, UserManager<User> userManager, SignInManager<User> signInManager, ILogger<AuthController> logger)
        {
            _configuration = configuration;
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] LoginRequest model)
        {
            string token = "";
            if (model is null)
            {
                throw new ArgumentNullException(nameof(model));
            }

            if (string.IsNullOrEmpty(model.Email))
            {
                return BadRequest("Email is required.");
            }

            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                return BadRequest("Invalid email or password.");
            }

            if (string.IsNullOrEmpty(model.Password))
            {
                return BadRequest("Password is required.");
            }

            var result = await _signInManager.PasswordSignInAsync(user, model.Password, false, false);
            if (!result.Succeeded)
            {
                return BadRequest("Invalid email or password.");
            }

            if (user != null && !string.IsNullOrEmpty(user.Token) && !IsTokenExpired(user.Token))
            {
                // Token is valid, return it to the user
                token = user.Token;
                _logger.LogInformation("Returning existing token for user: {Email}", user.Email);
            }
            else
            {
                token = GenerateJwtToken(user, _configuration["JwtSettings:Audience"]);
                _logger.LogInformation("Generating new token for user: {Email}", user.Email);
            }

            // Include the token in the response headers
            Response.Headers.Add("Authorization", "Bearer " + token);

            user.Token = token;
            await _userManager.UpdateAsync(user);
            return Ok(new { Token = token });
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="audience"></param>
        /// <returns></returns>

        private string GenerateJwtToken(User user, string audience)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var secretKey = _configuration["JwtSettings:SecretKey"];
            var issuer = _configuration["JwtSettings:Issuer"];



            if (secretKey is null)
            {
                throw new ArgumentNullException("JwtSettings:SecretKey", "JWT Secret Key is not configured.");
            }

            var key = Encoding.ASCII.GetBytes(secretKey);

            // Get user roles from the database or any other source
            var roles = _userManager.GetRolesAsync(user).Result;

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim("aud", audience),
                new Claim("issuer", issuer)
            };

            // Add user roles as claims
            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                Issuer = issuer
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
        private bool IsTokenExpired(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;
            var expirationDate = jwtToken?.ValidTo;

            return expirationDate.HasValue && expirationDate.Value < DateTime.UtcNow;
        }
    }
}
